# Sawfly: A Home Router UI for Debian


## Maintain basic router config

 - Use standard Debian config files to configure the system.
 - This includes /etc/network/interfaces
 - Sysctl to enable packet forwarding

## Provide packet processing rules with nftables

 - https://wiki.nftables.org
 - https://wiki.debian.org/nftables
 
 - Firewall and NAT (no extra tool, for now)
 - 

## Provide standard home router functionality with standard tools

 - dnsmasq looks like it provides DNS and DHCP
 - need to figure out 
 - nginx reverse-proxies to multiple web apps
 - inadyn? for dyn dns

## Other ideas

 - https://wiki.debian.org/FreedomBox/Design/Router

## Config file management


# Default Phoenix README

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

