defmodule SawflyWeb.PageController do
  use SawflyWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
