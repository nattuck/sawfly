defmodule Sawfly.Repo do
  use Ecto.Repo,
    otp_app: :sawfly,
    adapter: Ecto.Adapters.Postgres
end
